extern crate redux;
extern crate xmlparser as xml;
extern crate flate2;
extern crate arrayvec;

use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::io;
//use redux::model::*;
use flate2::read::GzDecoder;
use arrayvec::ArrayVec;

// struct ProbabilityRange {
//     begin: f64,
//     end: f64,
// }

#[derive(Clone)]
struct Atom {
    id: u32,
    name: String,
    res: String,
    chain:String,
    coord: ArrayVec<[f32; 3]>,
}

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("Usage: encode file.xml");
        return;
    }

    let raw_text = match load_file(&args[1]) {
        Ok(text) => text,
        Err(_) => panic!("Error!")
    };

    println!("Resulting data is {}", raw_text.len());

    let mut sequence = Vec::new();
    //let mut res = xml::StrSpan::from_substr("", 0, 0);
    let mut res = String::new();
    let mut atom = Atom {
        id: 0,
        name: String:: from(""),
        res: String:: from(""),
        chain: String::from(""),
        coord: ArrayVec::from([0.0, 0.0, 0.0])
    };
    for token in xml::Tokenizer::from(&raw_text[..]) {
        //println!("{:?}", token);
        match token {
            // Ok(xml::Token::ElementStart(x, y)) => {
            //     println!("<{}>", y);
            // },
            Ok(xml::Token::ElementEnd(xml::ElementEnd::Close(_, y))) => {
                match y.to_str() {
                    "Cartn_x" => {
                        atom.coord[0] = res.parse::<f32>().unwrap();
                    },
                    "Cartn_y" => {
                        atom.coord[1] = res.parse::<f32>().unwrap();
                    },
                    "Cartn_z" => {
                        atom.coord[2] = res.parse::<f32>().unwrap();
                    },
                    "label_atom_id" => {
                        atom.name = res.clone();
                    },
                    "label_comp_id" => {
                        atom.res = res.clone();
                    },
                    "label_seq_id" => {
                        atom.id = res.parse::<u32>().unwrap();
                    },
                    "label_asym_id" => {
                        atom.chain = res.clone();
                    },
                    "atom_site" => {
                        sequence.push(atom.clone());
                    },
                    &_ => (),
                }
            },
            Ok(xml::Token::Text(t)) => {
                res = String::from(t.to_str());
            },
            Ok(_) => (),
            Err(_) => (),
        }
    }

    //Filter by C_alpha
    let cas:Vec<&Atom> = sequence.iter()
        .filter(|r| r.name == "CA" && r.chain == "B").collect();

    println!("Found {} CA residues", cas.len());
    for res in cas {
        println!("{}: {}", res.id, res.res);
    }
    
        /*
    let mut buffer = String::new();
    {
        let mut f = File::open("input.txt").expect("File not found");
        f.read_to_string(&mut buffer).expect("Error reading file");
    } else {
        buffer = args[1].clone() + "\n";
    }
    
    let code = encode(buffer.bytes());
    println!("Encoded: 0x{:x?}\nDec: {}Exp: {}", code.to_bits(), decode(code), buffer);
    println!("0x{:x?}", encode2(buffer.bytes()));

    let mut cursor1 = std::io::Cursor::new(buffer.into_bytes());
    let mut compressed = Vec::<u8>::new();
    let result = match redux::compress(&mut cursor1, &mut compressed, AdaptiveTreeModel::new(Parameters::new(8, 14, 16).unwrap())) {
        Ok(v) => println!("Decom: {} | Com:{}", v.0, v.1),
        Err(e) => panic!("Error!")
    };
    println!("Result: {:?}", compressed);
*/
}

fn load_file(path: &str) -> io::Result<String> {
    let file = File::open(path).unwrap();
    let mut text = String::new();
    let mut gz = GzDecoder::new(file).expect("Couldn't decode gzip stream");
    gz.read_to_string(&mut text)?;
    Ok(text)
}

/*
fn encode(input: std::str::Bytes) -> f64 {
    let mut high: f64 = 1.0;
    let mut low: f64 = 0.0;

    for b in input {
        let p = get_probability(b);
        let range = high - low;
        high = low + range * p.end;
        low = low + range * p.begin;
    }

    low + (high - low) / 2.0
}

fn encode2(input: std::str::Bytes) -> u64 {
    let high: u64 = u64::max_value() >> 1;
    let low: u64 = 0;
    high - low + 1
}

fn decode(message: f64) -> String {
    let mut high: f64 = 1.0;
    let mut low: f64 = 0.0;
    let mut result = String::new();

    loop {
        let range = high - low;
        let c = get_symbol((message - low) / range);
        result.push(c);
        if c == '\n' {
            break;
        }

        let p = get_probability(c as u8);
        high = low + range * p.end;
        low = low + range * p.begin;
    }

    result
}

fn get_probability(c: u8) -> ProbabilityRange {
    let val: char = c as char;
    match val {
        'A' => ProbabilityRange {begin: 0.0, end: 0.2},
        'B' => ProbabilityRange {begin: 0.2, end: 0.4},
        'C' => ProbabilityRange {begin: 0.4, end: 0.6},
        'D' => ProbabilityRange {begin: 0.6, end: 0.7},
        'E' => ProbabilityRange {begin: 0.7, end: 0.8},
        'F' => ProbabilityRange {begin: 0.8, end: 0.9},
        '\n' => ProbabilityRange {begin: 0.9, end: 1.0},
        _ => ProbabilityRange {begin: 1.0, end: 1.0},
    }
}

fn get_symbol(d: f64) -> char {
    if d >= 0.0 && d < 0.2 {
        'A'
    } else if d >= 0.2 && d < 0.4 {
        'B'
    } else if d >= 0.4 && d < 0.6 {
        'C'
    } else if d >= 0.6 && d < 0.7 {
        'D'
    } else if d >= 0.7 && d < 0.8 {
        'E'
    } else if d >= 0.8 && d < 0.9 {
        'F'
    } else {
        '\n'
    }
}
*/
